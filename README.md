# Cisco Configuration Cheat Sheet

**[Cisco Configuration Cheat Sheet - html version](https://nerds-in-the-corner.gitlab.io/Cisco-cheat-sheet/)**

* [Introduction](../docs/index.md)
* [Basic Commands](../docs/basic_commands.md)
* [Switches](../docs/switches.md)
* [Routers](../docs/routers.md)
* [Routing Protocols](../docs/routing.md)
* [Network Security and Securing Devices](../docs/security.md)
* [Subnetting Cheat Sheet](../docs/subnetting.md)

---

* Want to contribute? Make a pull request to the
[Cisco-cheat-sheet](https://gitlab.com/nerds-in-the-corner/Cisco-cheat-sheet)

---
