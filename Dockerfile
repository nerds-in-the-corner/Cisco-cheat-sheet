FROM python

WORKDIR /usr/src/app
EXPOSE 8000
VOLUME /usr/src/app

RUN pip install mkdocs && \
    pip install mkdocs-material && \
    pip install pygments

ENTRYPOINT [ "mkdocs", "serve", "--dev-addr", "0.0.0.0:8000"]
