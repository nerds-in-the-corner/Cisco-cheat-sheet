# Switches

## vLANs

### Make vLAN

```cisco
switch> enable
switch# configure terminal
switch(config)# vlan <vlan id>
Switch(config-vlan)# name <vlan name>
```

### Set vlan on interface

```cisco
switch> enable
switch# configure terminal
switch(config)# interface <interface>
switch(config-if)# switchport mode access
switch(config-if)# switchport access vlan<vlan id>
```

### Trunk vLANs

```cisco
switch> enable
switch# configure terminal
switch(config)# interface <interface>
switch(config-if)# switchport mode trunk
switch(config-if)# switchport trunk allowed vlan [add, all, remove, none] <vlan id>
```

### Show all vLANs

```cisco
switch> enable
switch# show vlans
```

## Create and setup port channel on interface

### Create a port-channel

```cisco
switch> enable
switch# configure terminal
Switch(config)# interface port-channel <NUMBER>  
```

### Add port-channel to a interface 
Add the port-channe to each of the interfaces you want in the port-channel

```cisco
switch> enable
switch# configure terminal
switch(config)# interface <interface>
switch(config-if)# channel-group <NUMBER> mode <MODE>
switch(config-if)# exit
```

!!! Tip
	Don't add config to each interface, but rater add config to each port-channel


### Verify link aggregation

```cisco
switch> enable
switch# show port-channel summary

switch# show interface port-channel <NUMBER>
```

## Configure Port-channel with trunking

```cisco
switch> enable
switch# configure interface
switch(config)# interface port-channel <NUMBER>
switch(config-if)# switchport mode trunk
```

