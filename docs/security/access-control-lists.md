## ACL (access-control lists)

### New access list

```cisco
Router> enable
Router# configure terminal

// The access lists from 1-99 are basic lists, and it is recommended to use the extended access lists (100 and above) to have more options rules

Router(config)# access-list <100 - > [permit, deny, remark] <proto> host <source> host <dest> [eq, range] <port> <port>
```

### Show current access lists

```cisco
Router> enable
Router# show access-list
```

### Delete ACL group

```cisco
Router> enable
Router# configure terminal
Router(config)# ip access-list [standard, extended] <ACL group id>
Router(config-ext-nacl)# no <ACL group id>
```

### Add new rule to specific ACL id

```cisco
Router> enable
Router# configure terminal
Router(config)# ip access-list <standard, extended> <ACL group id>
Router(config-ext-nacl)# <Line id> [permit, deny, remark] <proto> host <source> host <dest> [eq, range] <port> <port>
```

### Enable ACL on interface

```cisco
Router> enable
Router# configure terminal
Router(config)# interface fastethernet0/0
Router(config-if)# ip access-group <ACL groupe ID>
```