# Remote access

## Activate SSH

#### Check list

- [Change hostname](../basic_commands#set-hostname-on-device) to something else than the default one before generating an RSA key
- [Add a new user](#add-a-new-user-with-password) to be used for logging in

### Generate RSA keys

```cisco
switch> enable
switch# configure terminal
switch(config)# ip domain-name <domain-name>
switch(config)# crypto key generate rsa
```

### Enable SSH version 2

**[Generate RSA Keys](#generate-rsa-keys) before enabling SSH version 2**

```cisco
switch> enable
switch# configure terminal
switch(config)# ip ssh version 2
```

### Set password on SSH

```cisco
switch> enable
switch# configure terminal
switch(config)# line vty 0 4
switch(config-line)# password <password>
switch(config-line)# login local
```

### Enable SSH on VTYs

```cisco
switch> enable
switch# configure terminal
switch(config)# line vty 0 4
switch(config-line)# transport input ssh
```