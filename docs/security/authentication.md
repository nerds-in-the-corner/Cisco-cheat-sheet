# Authentication

## Usernames and passwords

### Add a new user with password

```cisco
switch> enable
switch# configure terminal
switch(config)# username <username> secret <password>
```

### Set password on enable mode

```cisco
switch> enable
switch# configure terminal
switch(config)# enable secret <password>
```

### Scramble plaintext password in config

```cisco
switch> enable
switch# configure terminal
switch(config)# service password-encryption
```

## AAA (Authentication, Authorization and Accounting)

### RADIUS (Remote Authentication Dial-In User Service)

#### RADIUS and SSH

**[Set up a domain name and generate an RSA key](#generate-rsa-keys), [enable SSH version 2](#enable-ssh-version-2) and [enable SSH on the VTYs](#enable-ssh-on-vtys) before continuing**

```cisco
Router> enable
Router# configure terminal
Router(config)# ntp update-calendar
Router(config)# aaa new-model
Router(config)# aaa authentication login default group radius local
Router(config)# aaa authentication login <word> group radius
Router(config)# aaa authorization exec <word/default> start-stop group radius
Router(config)# radius-server host <server ip> auth-port <port number> key <password>
Router(config)# radius-server key <password>
Router(config)# line vty 0 4
Router(config-line)# login authentication <group name>
Router(config-line)# accounting exec default
Router(config-line)# exit
```

### TACACS (Terminal Access Controller Access-Control System)

* Nothing here yet... Want to contribute? Make a pull request to the
  [Cisco-cheat-sheet](https://gitlab.com/nerds-in-the-corner/Cisco-cheat-sheet)

## LAN-to-LAN IPsec Tunnel (pre-shared key)

```cisco
Router A:

Router> enable
Router# configure terminal

// Configure Isakmp
Router(config)# crypto isakmp policy <Number>
Router(config-isakmp)# hash <md5/sha>
Router(config-isakmp)# authentication pre-share
Router(config-isakmp)# exit

// Configure Crypto Map
Router(config)# crypto isakmp key vpnuser address <address>
Router(config)# crypto ipsec transform-set <WORD> <choise of transform> <choise of transform>
Router(config)# crypto map <word> <Number> ipsec-isakmp
Router(config-crypto-map)# set peer <IP for WAN acces router site 2>
Router(config-crypto-map)# set transform-set <WORD>
Router(config-crypto-map)# match address <NUMBER/WORD>
Router(config-crypto-map)# exit

Router(config)# interface <interface>
Router(config-if)# crypto map <WORD>
Router(config-if)# exit
Router(config)# access-list <NUMBER/WORD> permit <LAN IP site 1 wildcard> <LAN IP site 2 wildcard>

// For router B swap the sites and repeat
```
